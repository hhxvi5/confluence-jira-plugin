package com.atlassian.confluence.plugins.sprint.event;

import com.atlassian.analytics.api.annotations.EventName;

@EventName("confluence.macro.sprint.view")
public class SprintMacroViewAnalyticEvent
{
}
